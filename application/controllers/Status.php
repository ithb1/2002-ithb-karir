<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {
	
	var $kelas = "Status";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

	}

	public function index(){
		$data["rowData"] = $this->M_mst_status->getAll("ASC");
		$data['konten'] = "status/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_mst_status->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["nama"] = $this->input->post("nama");
		$data["step"] = $this->input->post("step");

		if($id) 
			$this->M_mst_status->update($id,$data);
		else 
			$this->M_mst_status->add($data);

		redirect($this->kelas);
	}

	public function delete($id){		
		$this->M_mst_status->delete($id);
		redirect($this->kelas);
	}
}
