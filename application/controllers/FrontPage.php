<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontPage extends CI_Controller {

    var $kelas = "FrontPage";

	function __construct(){
		parent::__construct();
	}

	public function index()
	{
        $data["dataLowongan"] = $this->M_lowongan->getAllBy("status = 'APPROVED'");
		$data["konten"] = "front/index";
		$this->load->view('templateFront',$data);
	}

    public function search(){
        $token = $this->input->post("token");

        $pelamar = $this->M_pelamar->getDetailBy("token = '$token'");
        if($pelamar){
            $pelamarPengalaman = $this->M_pelamar_pengalaman->getAllByFk($pelamar->id);
            $pelamarPendidikan = $this->M_pelamar_pendidikan->getAllByFk($pelamar->id);

            $data['rowStatus'] = $this->M_mst_status->getAllBy("step > 0", "ASC");
            $data['pelamar'] = $pelamar;
            $data['pelamarPengalaman'] = $pelamarPengalaman;
            $data['pelamarPendidikan'] = $pelamarPendidikan;
            $data['konten'] = "front/profil";
        }
        else{
            $data['konten'] = "front/error_search";
        }
        $this->load->view('templateFront',$data);
    }

    public function register($id)
    {
        $data['lowongan'] = $this->M_lowongan->getDetail($id);
        $data['konten'] = "front/register";
        $this->load->view('templateFront',$data);
    }

    public function onboarding($pelamarid)
    {
        $data['pelamarid'] = $pelamarid;
        $data["rowMateri"] = $this->M_mst_materi->getAllOrder("urutan", "ASC");
        $data['konten'] = "front/onboarding";
        $this->load->view('templateFront',$data);
    }

    public function onboarding_post($id)
    {
        $this->M_pelamar->update($id,array("statusid" => '5'));
        $data['konten'] = "front/konfirmasi_onboarding";
        $this->load->view('templateFront',$data);
    }

    public function register_post()
    {
        $data["token"] = '';
        $tanggal_lahir = $this->input->post("tanggal_lahir");

        $date1=date_create($tanggal_lahir);
        $date2=date_create(date("Y-m-d"));
        $diff=date_diff($date1,$date2);
        $umur = $diff->format("%y");

//        insert pelamar
        $dataPelamar = array(
            'lowonganid' => $this->input->post("lowonganid"),
            'fullname' => $this->input->post("fullname"),
            'alamat' => $this->input->post("alamat"),
            'email' => $this->input->post("email"),
            'notelp' => $this->input->post("notelp"),
            'jenis_kelamin' => $this->input->post("jenis_kelamin"),
            'tempat_lahir' => $this->input->post("tempat_lahir"),
            'tanggal_lahir' => $tanggal_lahir,
            'umur' => $umur,
            'jenjang_pendidikan' => $this->input->post("jenjang_pendidikan"),
            'instansi_pendidikan' => $this->input->post("instansi_pendidikan"),
            'nilai_akhir' => $this->input->post("nilai_akhir"),
            'tahun_lulus' => $this->input->post("tahun_lulus"),
            'statusid' => "1",
        );

        $this->M_pelamar->add($dataPelamar);
        $pelamar = $this->M_pelamar->getLast();
        $kombinasiToken = $pelamar->lowonganid."-".$pelamar->id;
        $token = $this->encrypt->encode($kombinasiToken);

        if ($_FILES['file']['name']) {
            $uploadPath = "extras/upload/cv/";
            $filename = $this->unggah->upload_files('file', "", $uploadPath);
            $filetype = $_FILES['file']['type'];

            $dataUpdate = array(
                "cv" => str_replace(" ", "_", $filename),
                "token" => $token
            );
            $this->M_pelamar->update($pelamar->id, $dataUpdate);

    //        insert pendidikan
            $post_pendidikan = $this->input->post("arr_pendidikan");
            $arr_pendidikan = json_decode($post_pendidikan);
            if($arr_pendidikan){
                foreach ($arr_pendidikan as $row){
                    $dataPendidikan = array(
                        'pelamarid' => $pelamar->id,
                        'pendidikan' => $row->pendidikan,
                        'nama' => $row->nama,
                        'jurusan' => $row->jurusan,
                        'nilai' => $row->nilai,
                        'tahun_lulus' => $row->tahun_lulus,
                    );
                    $this->M_pelamar_pendidikan->add($dataPendidikan);
                }
            }

    //        insert pengalaman
            $post_pengalaman = $this->input->post("arr_pengalaman");
            $arr_pengalaman = json_decode($post_pengalaman);
            if($arr_pengalaman){
                foreach ($arr_pengalaman as $row){
                    $dataPengalaman = array(
                        'pelamarid' => $pelamar->id,
                        'organisasi' => $row->organisasi,
                        'jabatan' => $row->jabatan,
                        'start_date' => $row->start_date,
                        'end_date' => $row->end_date,
                    );
                    $this->M_pelamar_pengalaman->add($dataPengalaman);
                }

            }

            $data["token"] = $token;
        } else{
            $filename = '';
            $filetype = '';
//            $this->session->set_flashdata("warning","File gagal disimpan");
//            $this->session->unset_flashdata("success");
            $this->M_pelamar->delete($pelamar->id);
        }

        $data["konten"] = "front/konfirmasi_join";
        $this->load->view('templateFront',$data);
    }
}
