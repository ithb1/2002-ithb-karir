<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materi extends CI_Controller {
	
	var $kelas = "Materi";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

	}

	public function index(){
		$data["rowData"] = $this->M_mst_materi->getAllOrder("urutan", "ASC");
		$data['konten'] = "materi/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_mst_materi->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["judul"] = $this->input->post("judul");
		$data["deskripsi"] = $this->input->post("deskripsi");
		$data["urutan"] = $this->input->post("urutan");
		$data["tipe"] = $tipe = $this->input->post("tipe");

		if($id) 
			$this->M_mst_materi->update($id,$data);
		else {
            $this->M_mst_materi->add($data);
            $id = $this->M_mst_materi->getMax("id");
        }

		if($tipe == 2) redirect("MateriDetail/index/".$id);
		else redirect($this->kelas);
	}

	public function delete($id){		
		$this->M_mst_materi->delete($id);
		redirect($this->kelas);
	}
}
