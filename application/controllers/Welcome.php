<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		 if (!$this->session->userdata("id")){
		 	redirect("FrontPage");
		 }

		$id = $this->session->userdata("id");
		$this->user = $this->M_user->getDetail($id);
	}

	public function index()
	{
//        $this->session->set_userdata("id",1);
        $userid = $this->user->userid;
        $data['rowStatus'] = $this->M_mst_status->getAllOrder("id","ASC");
		$data["rowLowonganReject"] = $this->M_lowongan->getAllBy("status = 'REJECTED' AND userid = $userid");
		$data["rowLowonganDashboard"] = $this->M_lowongan->getAllBy("status = 'APPROVED' AND userid = $userid");
		$data["rowLowongan"] = $this->M_lowongan->getAllBy("status = 'NEW' AND userid = $userid");
		$data["konten"] = "dashboard";
		$this->load->view('template',$data);
	}

	public function profil(){
	    if($this->input->post("btnsubmit")){
			$data["nama"] = $this->input->post("nama");
			if($this->input->post("password")){
				$data["password"] = $this->encrypt->encode($this->input->post("password"));
			}
			$this->M_user->update($this->user->userid,$data);
			redirect("Welcome/profil");
	    }
		
		$data['user'] = $this->user;
		$data['konten'] = "profil";
		$this->load->view('template',$data);
	}

	public function export(){
        $data['filename'] = "Laporan";
		$data["rowData"] = $this->M_user->getAll();
        $data['konten'] = "page/export/user";
        $this->load->view("page/export/templatePdf",$data);
	}
}
