<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminPanel extends CI_Controller {

	function __construct(){
        parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

	public function index(){
        $data["rowData"] = $this->M_history_user->getTop();
		$data["konten"] = "dashboard";
		$this->load->view('template',$data);
	}
}
