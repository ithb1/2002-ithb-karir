<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MateriDetail extends CI_Controller {
	
	var $kelas = "MateriDetail";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

	}

	public function index($materiId){
		$data["dataMateri"] = $this->M_mst_materi->getDetail($materiId);
		$data["rowData"] = $this->M_mst_materi_detail->getAllBy("materiid = $materiId");
		$data['konten'] = "materi/detail";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_mst_materi_detail->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["materiid"] = $materiid = $this->input->post("materiid");
		$data["jawaban"] = $this->input->post("jawaban");
		$data["istrue"] = $this->input->post("istrue");

		if($id) 
			$this->M_mst_materi_detail->update($id,$data);
		else {
            $this->M_mst_materi_detail->add($data);
            $id = $this->M_mst_materi_detail->getMax("id");
        }

		redirect($this->kelas.'/index/'.$materiid);
	}

	public function setTrue($id){
        $materiDetail = $this->M_mst_materi_detail->getDetail($id);
        $jawaban = $this->M_mst_materi_detail->getAllBy("materiid = $materiDetail->materiid");
        foreach ($jawaban as $item){
            $this->M_mst_materi_detail->update($item->id,array("istrue" => 0));
        }

        $this->M_mst_materi_detail->update($id,array("istrue" => 1));

		redirect($this->kelas.'/index/'.$materiDetail->materiid);
	}

	public function delete($id){
        $materi = $this->M_mst_materi_detail->getDetail($id);
		$this->M_mst_materi_detail->delete($id);
        redirect($this->kelas.'/index/'.$materi->id);
	}
}
