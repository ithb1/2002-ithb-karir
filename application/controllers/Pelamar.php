<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelamar extends CI_Controller {
	
	var $kelas = "Pelamar";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);

	}

	public function index($id, $statusid = "ALL"){
	    $status = "";
	    if($statusid <> "ALL") $status = "AND statusid = $statusid";
        $data["dataLowongan"] = $dataLowongan = $this->M_lowongan->getDetail($id);
        if($dataLowongan->jenis_kelamin == "a") $jeniskelamin = "AND (jenis_kelamin = 'l' OR jenis_kelamin = 'p')";
        else $jeniskelamin = "AND jenis_kelamin = '$dataLowongan->jenis_kelamin'";
        $where = "lowonganid = $id $status $jeniskelamin AND jenjang_pendidikan = '$dataLowongan->jenjang_pendidikan' AND nilai_akhir >= CAST($dataLowongan->nilai_akhir AS decimal(10,2)) AND umur <= $dataLowongan->umur";
        $data["rowDataPrioritas"] = $this->M_pelamar->getAllBy($where);
        $data["rowDataNonPrioritas"] = $this->M_pelamar->getAllBy("lowonganid = $id $status AND id NOT IN (SELECT id FROM pelamar WHERE $where)");
		$data["rowStatus"] = $this->M_mst_status->getAll("ASC");
		$data['konten'] = "pelamar/index";
		$data['statusid'] = $statusid;
		$this->load->view('template',$data);
	}

    public function filter($lowonganid){
        $statusid = $this->input->post("statusid");
        redirect($this->kelas."/index/".$lowonganid.'/'.$statusid);
    }

	public function detailPage($id){
		$data["data"] = $this->M_pelamar->getDetail($id);
		$data["dataPendidikan"] = $this->M_pelamar_pendidikan->getDetailByFk($id);
		$data["dataPengalaman"] = $this->M_pelamar_pengalaman->getDetailByFk($id);
		$data['konten'] = "pelamar/detail";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_pelamar->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["userid"] = $this->user->userid;
		$data["jabatanid"] = $this->input->post("jabatanid");
		$data["peranid"] = $this->input->post("peranid");
		$data["judul"] = $this->input->post("judul");
		$data["deskripsi"] = $this->input->post("deskripsi");
		$data["jumlah"] = $this->input->post("jumlah");
		$data["jenis_kelamin"] = $this->input->post("jenis_kelamin");
		$data["jenjang_pendidikan"] = $this->input->post("jenjang_pendidikan");
		$data["nilai_akhir"] = $this->input->post("nilai_akhir");
		$data["start_date"] = $this->input->post("start_date");
		$data["end_date"] = $this->input->post("end_date");

		if($id)
            $this->M_pelamar->update($id,$data);
        else {
            $data["status"] = "NEW";
            $this->M_pelamar->add($data);
        }

		redirect($this->kelas."/index/".$id);
	}

	public function setStatus(){
        $lowonganid = $this->input->post("lowonganid");
        $infotambahan = $this->input->post("infotambahan");
		$id = $this->input->post("id");
		$data["statusid"] = $statusid = $this->input->post("statusid");
        $this->M_pelamar->update($id,$data);
//        $this->xemail->send($id, $statusid, $infotambahan);
		redirect($this->kelas."/index/".$lowonganid);
	}

	public function delete($id){		
		$this->M_pelamar->delete($id);
		redirect($this->kelas."/index/".$id);
	}
}
