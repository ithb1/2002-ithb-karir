<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lowongan extends CI_Controller {
	
	var $kelas = "Lowongan";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);

	}

	public function index(){
		$data["rowData"] = $this->M_lowongan->getAll();
		$data["rowJabatan"] = $this->M_mst_jabatan->getAll();
		$data["rowPeran"] = $this->M_mst_peran->getAll();
		$data['konten'] = "lowongan/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_lowongan->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["userid"] = $this->user->userid;
		$data["jabatanid"] = $this->input->post("jabatanid");
		$data["peranid"] = $this->input->post("peranid");
		$data["judul"] = $this->input->post("judul");
		$data["deskripsi"] = $this->input->post("deskripsi");
		$data["jumlah"] = $this->input->post("jumlah");
		$data["jenis_kelamin"] = $this->input->post("jenis_kelamin");
		$data["umur"] = $this->input->post("umur");
		$data["jenjang_pendidikan"] = $this->input->post("jenjang_pendidikan");
		$data["nilai_akhir"] = $this->input->post("nilai_akhir");
		$data["start_date"] = $this->input->post("start_date");
		$data["end_date"] = $this->input->post("end_date");

		if($id)
            $this->M_lowongan->update($id,$data);
        else {
            $data["status"] = "NEW";
            $this->M_lowongan->add($data);
        }

		redirect($this->kelas);
	}

	public function setStatus(){
		$id = $this->input->post("id");
		$data["status"] = $this->input->post("status");
		$data["alasan"] = $this->input->post("alasan");
        $this->M_lowongan->update($id,$data);
		redirect($this->kelas);
	}

	public function delete($id){		
		$this->M_lowongan->delete($id);
		redirect($this->kelas);
	}
}
