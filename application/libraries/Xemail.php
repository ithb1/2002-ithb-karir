<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xemail {

    public function __get($var) {
        return get_instance()->$var;
    }

    public function send($pelamarid, $statusid, $infotambahan) {
        $this->load->config('email');

        $pelamar = $this->M_pelamar->getDetail($pelamarid);
        $status = $this->M_mst_status->getDetail($statusid);

        $from = $this->config->item('smtp_user');
        $to = $pelamar->email;
        $subject = "[no-reply] APLIKASI KARIR";
        $message = $status->deskripsi."\n *notes : $infotambahan";

        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
//            echo 'Your Email has successfully been sent.';
        } else {
            show_error($this->email->print_debugger());
        }
    }

}
