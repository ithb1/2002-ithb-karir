<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/*
 * Version: 0.7
 */

class Unggah {

    public function __construct() {
        $this->load->library('upload');
        $this->load->library('image_lib');
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    /*
     * Upload Picture
     */

    public function upload_foto($inputFile, $name, $uploadPath, $thumbnail = '', $width = '300', $height = '300') {
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = "gif|jpg|jpeg|png";
        $config['max_size'] = "5000";
        $config['file_ext_tolower'] = TRUE;
//        $config['encrypt_name'] = TRUE;
        $config['overwrite'] = TRUE;
//        $config['max_filename'] = "100";
//        $config['max_width'] = "1024";
//        $config['max_height'] = "768";
        $config['file_name'] = $name;
        $this->upload->initialize($config);

        $this->create_directory($uploadPath);

        if (!$this->upload->do_upload($inputFile))
        {
            return FALSE;
        }
        else
        {
            $finfo = $this->upload->data();
            $pic = $finfo['file_name'];

            if ($thumbnail == TRUE)
            {
                $this->create_thumbnail($pic, $uploadPath, $width, $height);
            }

            return $pic;
        }
    }

    /*
     * Upload Document
     */

    public function upload_doc($inputFile, $name, $uploadPath) {
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = "pdf|doc|docx|xls|xlsx";
        $config['max_size'] = "10000";
        $config['file_ext_tolower'] = TRUE;
        $config['overwrite'] = TRUE;
        $config['max_filename'] = "100";
        $config['file_name'] = $name;
        $this->upload->initialize($config);

        $this->create_directory($uploadPath);

        if (!$this->upload->do_upload($inputFile))
        {
            return FALSE;
        }
        else
        {
            $finfo = $this->upload->data();
            $doc = $finfo['file_name'];
            return $doc;
        }
    }

    /*
     * Upload Files
     */

    public function upload_files($inputFile, $name, $uploadPath) {
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = "pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|zip|rar|cdr";
        $config['max_size'] = "100000";
        $config['file_ext_tolower'] = TRUE;
        $config['overwrite'] = TRUE;
        $config['file_name'] = $name;
        $this->upload->initialize($config);

        $this->create_directory($uploadPath);

        if (!$this->upload->do_upload($inputFile))
        {
            return FALSE;
        }
        else
        {
            $finfo = $this->upload->data();
            $file = $finfo['file_name'];
            return $file;
        }
    }

    /*
     * Create Thumbnail
     */

    public function create_thumbnail($name, $uploadPath, $width, $height) {
        $config['image_library'] = "gd2";
        $config['source_image'] = $uploadPath . $name;
        $config['new_image'] = $uploadPath . 'thumb/' . $name;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = FALSE;
        $config['file_ext_tolower'] = TRUE;
        $config['overwrite'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        $config['quality'] = "100";
        $this->image_lib->initialize($config);

        $this->create_directory($uploadPath . 'thumb/');

        if (!$this->image_lib->resize())
        {
            echo $this->image_lib->display_errors();
        }
    }

    /*
     * Delete File
     */

    public function delete_file($name, $uploadPath) {
        $filename = $uploadPath . $name;
        $thumbname = $uploadPath . 'thumb/' . preg_replace('/(^.*)(\.jpe?g|\.png|\.gif)(\?|$)/', '\\1_thumb\\2', $name);

        if (file_exists($filename))
        {
            unlink($filename);
        }

        if (file_exists($thumbname))
        {
            unlink($thumbname);
        }
        return FALSE;
    }

    /*
     * Move File
     */

//    public function move_file() {
//
//    }

    /*
     * Create Directory with 755 mode
     */
    public function create_directory($dir) {
        $dir_sub = explode("/", $dir);
        $sub = '';
        for ($i = 0; $i < count($dir_sub); $i++)
        {
            if (!is_dir($sub))
            {
                $sub = FCPATH . $dir_sub[0];
            }
            else
            {
                $sub = $sub . '/' . $dir_sub[$i] . '/';
            }

            if (!is_dir($sub))
            {
                mkdir("{$sub}", 0777, TRUE);
            }
        }
        $this->_create_file($dir);
    }

    /*
     * Create File index.html for security folder
     */

    private function _create_file($uploadPath) {
        $path = FCPATH . $uploadPath . "index.html";
        fopen($path, "w") or die("Unable to open file!");
    }

}
