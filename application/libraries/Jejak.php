<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jejak {

    var $table_name = "history_admin";

    public function __get($var) {
        return get_instance()->$var;
    }

    public function add($idPenerima, $aksi, $path) {
        date_default_timezone_set("Asia/Jakarta");
        $data = array(
            "idPenerima" => $idPenerima,
            "aksi" => $aksi,
            "waktu" => date("Y-m-d H-i-s"),
            "path" => $path,
            "statusNotif" => "0",
            );
        $this->db->insert($this->table_name, $data);
    }

    public function update($id) {
        $this->db->set("statusNotif", "1", FALSE);
        $this->db->where("idlog", $id);
        $this->db->update($this->table_name);
    }

    public function getDetail($id) {
        $this->db->where("idlog", $id);
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    public function getCount() {
        $userid = $this->session->userdata("id");
        return $this->db->query("SELECT COUNT(idlog) as id FROM $this->table_name WHERE statusNotif = '0' AND idPenerima = $userid")->row()->id;
    }

    public function getNotif() {
        $userid = $this->session->userdata("id");
        $this->db->where('idPenerima =', $userid);
        $this->db->order_by('idlog', 'DESC');
        $this->db->where("statusNotif", "0");
        return $this->db->get($this->table_name)->result();
    }

}
