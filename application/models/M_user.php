<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class M_user extends MY_Model {

    var $table_name = "sys_user";
    var $pk = "userid";
    var $user = "username";
    var $password = "password";


    function login($user,$pass) {
        $this->db->where($this->user, $user);
        $query = $this->db->get($this->table_name);
        if($query->num_rows()){
            $password = $query->row()->password;
            if($pass == $this->encrypt->decode($password))
                return $query->row();
        }
        return false;
    }
}