<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class M_pelamar_pendidikan extends MY_Model {

    var $table_name = "pelamar_pendidikan";
    var $pk = "id";
    var $fk = "pelamarid";
}