<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class M_pelamar_pengalaman extends MY_Model {

    var $table_name = "pelamar_pengalaman";
    var $pk = "id";
    var $fk = "pelamarid";
}