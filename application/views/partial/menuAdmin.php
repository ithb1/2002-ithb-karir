<?php
    $iduser = $this->session->userdata("id");
    $user = $this->M_user->getDetail($iduser);
?>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li class="<?=($this->uri->segment(1) == 'Welcome')?'active':''?>"><a href="<?=site_url('Welcome')?>"><i class="fa fa-home"></i> <span>Dashboard</span> </a></li>

  <li class="treeview <?=($this->uri->segment(1) == 'Jabatan'
    || $this->uri->segment(1) == 'Departemen'
    || $this->uri->segment(1) == 'Peran'
    || $this->uri->segment(1) == 'Materi'
    || $this->uri->segment(1) == 'Status'
    || $this->uri->segment(1) == 'User')?'active':''?>">
    <a href="#">
      <i class="fa fa-gear"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
      <li class="<?=($this->uri->segment(1) == 'Departemen')?'active':''?>"><a href="<?=site_url('Departemen')?>"><span>Departemen</span></a></li>
      <li class="<?=($this->uri->segment(1) == 'Jabatan')?'active':''?>"><a href="<?=site_url('Jabatan')?>"><span>Jabatan</span></a></li>
      <li class="<?=($this->uri->segment(1) == 'Peran')?'active':''?>"><a href="<?=site_url('Peran')?>"><span>Peran</span></a></li>
      <li class="<?=($this->uri->segment(1) == 'Materi')?'active':''?>"><a href="<?=site_url('Materi')?>"><span>Materi</span></a></li>
      <li class="<?=($this->uri->segment(1) == 'Status')?'active':''?>"><a href="<?=site_url('Status')?>"><span>Status</span></a></li>
      <li class="<?=($this->uri->segment(1) == 'User')?'active':''?>"><a href="<?=site_url('User')?>"><span>User</span></a></li>
    </ul>
  </li>
  <li class="<?=($this->uri->segment(1) == 'Lowongan')?'active':''?>"><a href="<?=site_url('Lowongan')?>"><i class="fa fa-briefcase"></i> <span>Lowongan</span></a></li>
</ul>