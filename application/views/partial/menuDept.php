<?php
    $iduser = $this->session->userdata("id");
    $user = $this->M_user->getDetail($iduser);
?>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li class="<?=($this->uri->segment(1) == 'Welcome')?'active':''?>"><a href="<?=site_url('Welcome')?>"><i class="fa fa-home"></i> <span>Dashboard</span> </a></li>
  <li class="<?=($this->uri->segment(1) == 'Lowongan')?'active':''?>"><a href="<?=site_url('Lowongan')?>"><i class="fa fa-briefcase"></i> <span>Lowongan</span></a></li>
</ul>