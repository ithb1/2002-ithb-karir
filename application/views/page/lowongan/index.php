<?php
$iduser = $this->session->userdata("id");
$user = $this->M_user->getDetail($iduser);
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Lowongan
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Lowongan </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
          <div class="pull-right">
            <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalForm" onclick="clearForm()"><i class="fa fa-plus"></i> Tambah Data</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama Lowongan</th>
              <th>Pemohon</th>
              <th>Departemen</th>
              <th>Level</th>
              <th>Jabatan</th>
              <th>Jumlah</th>
              <th>Umur</th>
              <th>Status</th>
              <th>action</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach ($rowData as $row) :
              ?>
              <tr>
                <td><?=$no++;?></td>
                <td><?=$row->judul;?></td>
                <td><?=$this->M_user->getDetail($row->userid)->fullname;?></td>
                <td><?=$this->M_mst_departemen->getDetail($this->M_user->getDetail($row->userid)->departid)->nama;?></td>
                <td><?=$this->M_mst_jabatan->getDetail($row->jabatanid)->nama;?></td>
                <td><?=$this->M_mst_peran->getDetail($row->peranid)->nama;?></td>
                <td><?=$row->jumlah;?></td>
                <td><?=$row->umur;?></td>
                <td><label for="" class="label label-<?=($row->status == "NEW")?"primary":(($row->status == "APPROVED")?"warning":(($row->status == "REJECTED")?"danger":"success"));?>"><?=$row->status;?></label></td>
                <td>
                    <a href="<?=site_url('Pelamar/index/'.$row->id);?>" class="btn btn-xs btn-success"><i class="fa fa-users" title="pelamar"></i></a>
                    <?php if($user->roleid == "1"):?>
                    <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalStatus" onclick="getDetail(this)" class="btn btn-xs btn-info" title="set status"><i class="fa fa-tag"></i></a>
                    <?php endif;?>
                    <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalForm" onclick="getDetail(this)" class="btn btn-xs btn-warning" title="edit"><i class="fa fa-pencil"></i></a>
                  <a href="<?=site_url('Lowongan/delete/'.$row->id);?>" class="btn btn-xs btn-danger" title="hapus"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Lowongan</h4>
      </div>
      <?=form_open("Lowongan/add","class='form-horizontal'");
      ?>
      <div class="modal-body">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="judul" class="col-sm-4 control-label">Nama Lowongan <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                    <input type="text" class="form-control judul" placeholder="judul" name="judul" value="" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="jabatanid" class="col-sm-4 control-label">Level <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                      <select name="jabatanid" id="jabatanid" required class="form-control">
                          <option value="">- level -</option>
                          <?php foreach($rowJabatan as $row):?>
                              <option value="<?=$row->id?>" ><?=$row->nama?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="peranid" class="col-sm-4 control-label">Jabatan <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                      <select name="peranid" id="peranid" required class="form-control">
                          <option value="">- jabatan -</option>
                          <?php foreach($rowPeran as $row):?>
                              <option value="<?=$row->id?>" ><?=$row->nama?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="deskripsi" class="col-sm-4 control-label">Deskripsi <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="deskripsi" placeholder="deskripsi" name="deskripsi" value="" required>
                  </div>
                </div>
                  <div class="form-group">
                      <label for="jumlah" class="col-sm-4 control-label">Jumlah <i class="text-danger">*</i></label>
                      <div class="col-sm-8">
                          <input type="number" class="form-control" id="jumlah" placeholder="jumlah" name="jumlah" value="" step="0.01" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="jenis_kelamin" class="col-sm-4 control-label">Jenis Kelamin <i class="text-danger">*</i></label>
                      <div class="col-sm-8">
                          <select name="jenis_kelamin" id="jenis_kelamin" required class="form-control">
                              <option value="">- jenis kelamin -</option>
                              <option value="l">Laki-laki</option>
                              <option value="p">Perempuan</option>
                              <option value="a">Semua</option>
                          </select>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="jenjang_pendidikan" class="col-sm-4 control-label">Jenjang Pendidikan <i class="text-danger">*</i></label>
                      <div class="col-sm-8">
                          <select name="jenjang_pendidikan" id="jenjang_pendidikan" required class="form-control">
                              <option value="">- pendidikan -</option>
                              <option value="SMA">SMA/SMK/Sederajat</option>
                              <option value="S1">S1</option>
                          </select>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="peranid" class="col-sm-4 control-label">Nilai Akhir <i class="text-danger">*</i></label>
                      <div class="col-sm-8">
                          <input type="number" class="form-control" id="nilai_akhir" placeholder="nilai_akhir" name="nilai_akhir" value="" step="0.01" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="umur" class="col-sm-4 control-label">Umur <i class="text-danger">*</i></label>
                      <div class="col-sm-8">
                          <input type="number" class="form-control" id="umur" placeholder="umur" name="umur" value="" step="1" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="peranid" class="col-sm-4 control-label">Lowongan Dibuka <i class="text-danger">*</i></label>
                      <div class="col-sm-8">
                          <input type="date" class="form-control" id="start_date" placeholder="start_date" name="start_date" value="" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="peranid" class="col-sm-4 control-label">Lowongan Ditutup <i class="text-danger">*</i></label>
                      <div class="col-sm-8">
                          <input type="date" class="form-control" id="end_date" placeholder="end_date" name="end_date" value="" required>
                      </div>
                  </div>
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="modalStatus" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Lowongan</h4>
      </div>
      <?=form_open("Lowongan/setStatus","class='form-horizontal'");
      ?>
      <div class="modal-body">
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="judul" class="col-sm-4 control-label">Judul</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                    <input type="text" class="form-control judul" placeholder="judul" name="judul" value="" readonly>
                  </div>
                </div>
                  <div class="form-group">
                      <label for="status" class="col-sm-4 control-label">Status</label>
                      <div class="col-sm-8">
                          <select name="status" id="status" required class="form-control" onchange="cekStatus()">
                              <option value="">- status -</option>
                              <option value="NEW">NEW</option>
                              <option value="APPROVED">APPROVED</option>
                              <option value="DONE">DONE</option>
                              <option value="REJECTED">REJECTED</option>
                          </select>
                      </div>
                  </div>
                  <div class="form-group formreject" style="display: none">
                      <label for="status" class="col-sm-4 control-label">Alasan</label>
                      <div class="col-sm-8">
                          <input type="text" class="form-control alasan" placeholder="alasan" name="alasan" value="" >
                      </div>
                  </div>
              </div>
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
    var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>Lowongan/detail/"+id,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('.id').val(id).hide();
         $('#jabatanid').val(data.jabatanid);
         $('#peranid').val(data.peranid);
         $('.judul').val(data.judul);
         $('#deskripsi').val(data.deskripsi);
         $('#jumlah').val(data.jumlah);
         $('#jenis_kelamin').val(data.jenis_kelamin);
         $('#jenjang_pendidikan').val(data.jenjang_pendidikan);
         $('#umur').val(data.umur);
         $('#nilai_akhir').val(data.nilai_akhir);
         $('#start_date').val(data.start_date);
         $('#end_date').val(data.end_date);
         $('#status').val(data.status);
        }
    });
  }

  function clearForm() {    
     $('#id').val("");
     $('#jabatanid').val("");
     $('#peranid').val("");
     $('.judul').val("");
     $('#deskripsi').val("");
     $('#jumlah').val("");
     $('#jenis_kelamin').val("");
     $('#jenjang_pendidikan').val("");
     $('#umur').val("");
     $('#nilai_akhir').val("");
     $('#start_date').val("");
     $('#end_date').val("");
     $('#status').val("");
  }

  function cekStatus() {
      var status = $("#status").val();
      if(status == "REJECTED")
          $(".formreject").show();
      else
          $(".formreject").hide();
  }
</script>