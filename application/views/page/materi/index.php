
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Materi
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Materi </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
          <div class="pull-right">
            <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalForm" onclick="clearForm()"><i class="fa fa-plus"></i> Tambah Data</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Judul</th>
              <th>Urutan</th>
              <th>Tipe</th>
              <th>action</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach ($rowData as $row) :
              ?>
              <tr>
                <td><?=$no++;?></td>
                <td><?=$row->judul;?></td>
                <td><?=$row->urutan;?></td>
                <td><?=($row->tipe == 1)?"Materi":"Soal";?></td>
                <td>
                    <?php if($row->tipe == 2):?>
                        <a href="<?=site_url('MateriDetail/index/'.$row->id);?>" class="btn btn-xs btn-info"><i class="fa fa-list"></i></a>
                    <?php endif;?>
                  <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalForm" onclick="getDetail(this)" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                  <a href="<?=site_url('Materi/delete/'.$row->id);?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Materi</h4>
      </div>
      <?=form_open("Materi/add","class='form-horizontal'");
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="judul" class="col-sm-4 control-label">Judul</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                    <input type="text" class="form-control" id="judul" placeholder="judul" name="judul" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="tipe" class="col-sm-4 control-label">Tipe</label>
                  <div class="col-sm-8">
                      <label><input type="radio" name="tipe" value="1" onclick="$('.issoal').show()"> Materi</label>
                      <br>
                      <label><input type="radio" name="tipe" value="2" onclick="$('.issoal').hide()"> Soal</label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="urutan" class="col-sm-4 control-label">Urutan</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" id="urutan" placeholder="urutan" name="urutan" value="">
                  </div>
                </div>
                <div class="form-group issoal">
                  <label for="deskripsi" class="col-sm-4 control-label">Deskripsi</label>
                  <div class="col-sm-8">
                      <textarea name="deskripsi" class="form-control" id="deskripsi" cols="30" rows="10"></textarea>
                  </div>
                </div>
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
    var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>Materi/detail/"+id,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('#id').val(id).hide();
         $('#judul').val(data.judul);
         $('#deskripsi').val(data.deskripsi);
         $('#urutan').val(data.urutan);
         $('#tipe').val(data.tipe);
        }
    });
  }

  function clearForm() {    
     $('#id').val("");
     $('#judul').val("");
     $('#deskripsi').val("");
     $('#urutan').val("");
     $('#tipe').val("");
  }
</script>