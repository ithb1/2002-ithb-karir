<?php
    $warna[1] = "#f56954";
    $warna[2] = "#00a65a";
    $warna[3] = "#f39c12";
    $warna[4] = "#00c0ef";
    $warna[5] = "#3c8dbc";
    $warna[6] = "#d2d6de";
    $warna[7] = "#d81b60";
?>

<div class="row">
    <?php foreach ($rowLowonganReject as $item):?>
        <div class="alert alert-warning alert-dismissible">
            <h4><i class="icon fa fa-warning"></i> Warning!</h4>
            Lowongan <strong><?=$item->judul?></strong> di <strong>REJECT</strong> dengan alasan <?=$item->alasan?>
        </div>
    <?php endforeach;?>
    <?php foreach ($rowLowonganDashboard as $lowongan):?>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$lowongan->judul?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                <?php foreach ($rowStatus as $status):?>
                    <div class="col-md-1">
                        <!-- small box -->
                        <div class="small-box" style="background: <?=$warna[$status->id]?>">
                            <div class="inner">
                                <h3><?=$this->M_pelamar->getCountWhere(" lowonganid = $lowongan->id AND statusid = $status->id ")?></h3>
                                <p><?=$status->nama?></p>
                            </div>
                            <div class="icon">
            <!--                    <i class="ion ion-bag"></i>-->
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="chart-responsive">
                                    <canvas class="pieChart<?=$lowongan->id?>" height="155" width="328" style="width: 328px; height: 155px;"></canvas>
                                </div>
                                <!-- ./chart-responsive -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <ul class="chart-legend clearfix">
                                    <li><i class="fa fa-circle-o text-red"></i> <?=$this->M_mst_status->getDetail(1)->nama?></li>
                                    <li><i class="fa fa-circle-o text-green"></i> <?=$this->M_mst_status->getDetail(2)->nama?></li>
                                    <li><i class="fa fa-circle-o text-yellow"></i> <?=$this->M_mst_status->getDetail(3)->nama?></li>
                                    <li><i class="fa fa-circle-o text-aqua"></i> <?=$this->M_mst_status->getDetail(4)->nama?></li>
                                    <li><i class="fa fa-circle-o text-light-blue"></i> <?=$this->M_mst_status->getDetail(5)->nama?></li>
                                    <li><i class="fa fa-circle-o text-gray"></i> <?=$this->M_mst_status->getDetail(6)->nama?></li>
                                    <li><i class="fa fa-circle-o text-maroon"></i> <?=$this->M_mst_status->getDetail(7)->nama?></li>
                                </ul>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>

<script>
    $(function () {
        'use strict';
        //-------------
        //- PIE CHART -
        //-------------

        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 1,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: false,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
            //String - A tooltip template
            tooltipTemplate: "<%=value %> <%=label%> users"
        };

        <?php foreach ($rowLowonganDashboard as $lowongan):?>
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $(".pieChart<?=$lowongan->id?>").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [
                <?php foreach ($rowStatus as $status):?>
            {
                value: <?=$this->M_pelamar->getCountWhere(" lowonganid = $lowongan->id AND statusid = $status->id ")?>,
                color: "<?=$warna[$status->id]?>",
                highlight: "<?=$warna[$status->id]?>",
                label: "<?=$status->nama?>"
            },
                <?php endforeach;?>
        ];

        pieChart.Doughnut(PieData, pieOptions);
        <?php endforeach;?>
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        //-----------------
        //- END PIE CHART -
        //-----------------
    });
</script>