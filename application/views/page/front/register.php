<style>
    td{
        padding: 5px;
    }

    .stepwizard-step p {
        margin-top: 0px;
        color:#666;
    }
    .stepwizard-row {
        display: table-row;
    }
    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }
    .stepwizard-step button[disabled] {
        /*opacity: 1 !important;
        filter: alpha(opacity=100) !important;*/
    }
    .stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
        opacity:1 !important;
        color:#bbb;
    }
    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content:" ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-index: 0;
    }
    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }
    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
</style>

<!-- Main content -->
<section class="content">
  <div class="row">
      <div class="col-xs-12">
          <div class="box">
              <div class="box-header">
                  <h3>
                      <?=$lowongan->judul?>
                  </h3>
              </div>
              <div class="box-body">
                  <div class="stepwizard">
                      <div class="stepwizard-row setup-panel">
                          <div class="stepwizard-step col-xs-3">
                              <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                              <p><small>Personal Info</small></p>
                          </div>
                          <div class="stepwizard-step col-xs-3">
                              <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="">2</a>
                              <p><small>Education</small></p>
                          </div>
                          <div class="stepwizard-step col-xs-3">
                              <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="">3</a>
                              <p><small>Experience</small></p>
                          </div>
                          <div class="stepwizard-step col-xs-3">
                              <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="">4</a>
                              <p><small>FINISH</small></p>
                          </div>
                      </div>
                  </div>

                  <?=form_open_multipart("FrontPage/register_post")?>
                  <input type="hidden" name="lowonganid" value="<?=$lowongan->id?>" />
                      <div class="panel panel-primary setup-content" id="step-1">
                          <div class="panel-heading">
                              <h3 class="panel-title">Personal Info</h3>
                          </div>
                          <div class="panel-body">
                              <div class="form-group col-md-6">
                                  <label class="control-label">FullName <i class="text-danger">*</i></label>
                                  <input type="text" required="required" class="form-control" placeholder="Enter First" name="fullname" />
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">Jenjang Pendidikan <i class="text-danger">*</i></label>
                                  <select name="jenjang_pendidikan" id="jenjang_pendidikan" required class="form-control">
                                      <option value="">- pendidikan -</option>
                                      <option value="SMA">SMA/SMK/Sederajat</option>
                                      <option value="S1">S1</option>
                                  </select>
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">Alamat <i class="text-danger">*</i></label>
                                  <input type="text" required="required" class="form-control" placeholder="Enter Alamat" name="alamat" />
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">Instansi Pendidikan <i class="text-danger">*</i></label>
                                  <input type="text" required="required" class="form-control" placeholder="Enter Instansi Pendidikan" name="instansi_pendidikan" />
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">Email <i class="text-danger">*</i></label>
                                  <input type="email" required="required" class="form-control" placeholder="Enter Email" name="email" />
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">Tahun Lulus <i class="text-danger">*</i></label>
                                  <input type="number" required="required" class="form-control" placeholder="Enter Tahun Lulus" name="tahun_lulus" />
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">No Telepon <i class="text-danger">*</i></label>
                                  <input type="text" required="required" class="form-control" placeholder="Enter Telepon" name="notelp" />
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">Nilai AKhir <i class="text-danger">*</i></label>
                                  <input type="number" required="required" class="form-control" placeholder="Enter Nilai AKhir" name="nilai_akhir" step="0.01"/>
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">Tempat Lahir <i class="text-danger">*</i></label>
                                  <input type="text" required="required" class="form-control" placeholder="Enter Tempat Lahir" name="tempat_lahir" />
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">Jenis Kelamin <i class="text-danger">*</i></label>
                                  <select name="jenis_kelamin" id="jenis_kelamin" required class="form-control">
                                      <option value="">- jenis kelamin -</option>
                                      <option value="L">Laki-laki</option>
                                      <option value="P">Perempuan</option>
                                  </select>
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">Tanggal Lahir <i class="text-danger">*</i></label>
                                  <input type="date" required="required" class="form-control" placeholder="Enter Tanggal Lahir" name="tanggal_lahir" />
                              </div>
                              <div class="form-group col-md-6">
                                  <label class="control-label">CV <i class="text-danger">*</i></label>
                                  <input type="file" required="required" class="form-control" placeholder="Enter Tanggal Lahir" name="file" />
                              </div>
                          </div>
                          <div class="panel-footer" style="background: white">
                              <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                              <div class="clearfix"></div>
                          </div>
                      </div>

                      <div class="panel panel-primary setup-content" id="step-2">
                          <div class="panel-heading">
                              <h3 class="panel-title">Education</h3>
                          </div>
                          <div class="panel-body">
                              <a href="" class="pull-right btn btn-primary"  data-toggle="modal" data-target="#modalPendidikan" onclick="clearForm()">TAMBAH</a>
                              <table id="tabel-pendidikan" class="table table-bordered">
                                  <thead>
                                      <tr>
                                          <th>Pendidikan</th>
                                          <th>Nama</th>
                                          <th>Jurusan</th>
                                          <th>Tahun Lulus</th>
                                          <th>Nilai</th>
                                          <th>hapus</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                              </table>
                          </div>
                          <div class="panel-footer" style="background: white">
                              <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                              <div class="clearfix"></div>
                          </div>
                      </div>

                      <div class="panel panel-primary setup-content" id="step-3">
                          <div class="panel-heading">
                              <h3 class="panel-title">Experience</h3>
                          </div>
                          <div class="panel-body">
                              <a href="" class="pull-right btn btn-primary"  data-toggle="modal" data-target="#modalPengalaman" onclick="clearForm()">TAMBAH</a>
                              <table id="tabel-pengalaman" class="table table-bordered">
                                  <thead>
                                      <tr>
                                          <th>Organisasi</th>
                                          <th>Jabatan</th>
                                          <th>Start Date</th>
                                          <th>End Date</th>
                                          <th>hapus</th>
                                      </tr>
                                  </thead>
                                  <tbody></tbody>
                              </table>
                          </div>
                          <div class="panel-footer" style="background: white">
                              <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                              <div class="clearfix"></div>
                          </div>
                      </div>

                      <div class="panel panel-primary setup-content" id="step-4">
                          <div class="panel-heading">
                              <h3 class="panel-title">FINISH</h3>
                          </div>
                          <div class="panel-body">
                              <p>Klik Submit, dapatkan kode token dan tunggu proses selanjutnya</p>
                              <button class="btn btn-success pull-right" type="submit">Submit!</button>
                          </div>
                      </div>
                  <input type="hidden" name="arr_pendidikan" id="arr_pendidikan">
                  <input type="hidden" name="arr_pengalaman" id="arr_pengalaman">
                  <?=form_close()?>
              </div>
          </div>
      </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->


<!-- Modal -->
<div id="modalPendidikan" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Pendidikan</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row form-horizontal">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="roleid" class="col-sm-4 control-label">Pendidikan</label>
                                <div class="col-sm-8">
                                    <select name="fpendidikan" id="fpendidikan" required class="form-control">
                                        <option value="">- pendidikan -</option>
                                        <option value="SD">SD/MI/Sederajat</option>
                                        <option value="SMP">SMP/MTs/Sederajat</option>
                                        <option value="SMA">SMA/SMK/MA/Sederajat</option>
                                        <option value="S1">S1</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fnama" class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="fnama" placeholder="fnama" name="fnama" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fjurusan" class="col-sm-4 control-label">Jurusan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="fjurusan" placeholder="fjurusan" name="fjurusan" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fnilai" class="col-sm-4 control-label">Nilai</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="fnilai" placeholder="fnilai" name="fnilai" value="" step="0.01">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ftahun_lulus" class="col-sm-4 control-label">Tahun Lulus</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="ftahun_lulus" placeholder="ftahun_lulus" name="ftahun_lulus" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="addPendididkan()">submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
<div id="modalPengalaman" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Pengalaman</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row form-horizontal">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="forganisasi" class="col-sm-4 control-label">Organisasi</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="forganisasi" placeholder="forganisasi" name="forganisasi" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fjabatan" class="col-sm-4 control-label">Jabatan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="fjabatan" placeholder="fjabatan" name="fjabatan" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fstart_date" class="col-sm-4 control-label">Start_date</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" id="fstart_date" placeholder="fstart_date" name="fstart_date" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fend_date" class="col-sm-4 control-label">End_date</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" id="fend_date" placeholder="fend_date" name="fend_date" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="addPengalaman()">submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    var arr_pendidikan = [];
    var arr_pengalaman = [];

    $(document).ready(function () {

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-success').addClass('btn-default');
                $item.addClass('btn-success');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-success').trigger('click');
    });

    function clearForm() {
        $("#fpendidikan").val("");
        $("#fnama").val("");
        $("#fjurusan").val("");
        $("#fnilai").val("");
        $("#ftahun_lulus").val("");
        $("#forganisasi").val("");
        $("#fjabatan").val("");
        $("#fstart_date").val("");
        $("#fend_date").val("");
    }

    function addPendididkan() {
        var fpendidikan = $("#fpendidikan").val();
        var fnama = $("#fnama").val();
        var fjurusan = $("#fjurusan").val();
        var fnilai = $("#fnilai").val();
        var ftahun_lulus = $("#ftahun_lulus").val();

        var obj = {
            pendidikan : fpendidikan,
            nama : fnama,
            jurusan : fjurusan,
            nilai : fnilai,
            tahun_lulus : ftahun_lulus
        };

        arr_pendidikan.push(obj);
        $("#modalPendidikan").modal("hide");
        renderTablePendidikan();
    }

    function delPendidikan(index) {
        arr_pendidikan.splice(index,1);
        renderTablePendidikan();
    }

    function renderTablePendidikan(){
        var obj_pendidikan = JSON.stringify(arr_pendidikan);
        console.log(arr_pendidikan, obj_pendidikan);
        $("#arr_pendidikan").val(obj_pendidikan);
        $('#tabel-pendidikan tbody').empty();
        arr_pendidikan.forEach(function (item, index) {
            console.log(item);
            var res = "<tr>";
            res += "<td>"+item.pendidikan+"</td>";
            res += "<td>"+item.nama+"</td>";
            res += "<td>"+item.jurusan+"</td>";
            res += "<td>"+item.nilai+"</td>";
            res += "<td>"+item.tahun_lulus+"</td>";
            res += "<td><button class='btn btn-xs btn-danger' onclick='delPendidikan("+index+")'><i class='fa fa-trash'></i></button></td>";
            res += "</tr>";
            $('#tabel-pendidikan tbody').append(res);
        });
    }

    function addPengalaman() {
        var forganisasi = $("#forganisasi").val();
        var fjabatan = $("#fjabatan").val();
        var fstart_date = $("#fstart_date").val();
        var fend_date = $("#fend_date").val();

        var obj = {
            organisasi : forganisasi,
            jabatan : fjabatan,
            start_date : fstart_date,
            end_date : fend_date
        };

        arr_pengalaman.push(obj);
        $("#modalPengalaman").modal("hide");
        renderTablePengalaman();
    }

    function delPengalaman(index) {
        arr_pengalaman.splice(index,1);
        renderTablePengalaman();
    }

    function renderTablePengalaman(){
        var obj_pengalaman = JSON.stringify(arr_pengalaman);
        console.log(arr_pengalaman, obj_pengalaman);
        $("#arr_pengalaman").val(obj_pengalaman);
        $('#tabel-pengalaman tbody').empty();
        arr_pengalaman.forEach(function (item, index) {
            // console.log(item);
            var res = "<tr>";
            res += "<td>"+item.organisasi+"</td>";
            res += "<td>"+item.jabatan+"</td>";
            res += "<td>"+item.start_date+"</td>";
            res += "<td>"+item.end_date+"</td>";
            res += "<td><button class='btn btn-xs btn-danger' onclick='delPengalaman("+index+")'><i class='fa fa-trash'></i></button></td>";
            res += "</tr>";
            $('#tabel-pengalaman tbody').append(res);
        });
    }
</script>