
<!-- Main content -->
<section class="content">
  <div class="row">
      <div class="col-xs-12">
          <div class="box">
              <div class="box-body">
                  <div class="alert alert-success alert-dismissible">
                      <h4><i class="icon fa fa-check"></i> Selamat!</h4>
                      Selamat, anda telah menyelesaikan tahapan on boarding, tunggu konfirmasi selanjutnya.
                  </div>
              </div>
          </div>
      </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
