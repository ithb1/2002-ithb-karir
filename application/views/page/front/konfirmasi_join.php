
<!-- Main content -->
<section class="content">
  <div class="row">
      <div class="col-xs-12">
          <div class="box">
              <div class="box-body">
                  <?php if($token):?>
                  <div class="alert alert-success alert-dismissible">
                      <h4><i class="icon fa fa-check"></i> Sukses!</h4>
                        Data dan lamaran anda telah masuk pada sistem kami. Selanjutnya anda bisa melihat perkembangan data dan proses lamaran pada fitur <strong>'Detail Pelamar'</strong> dengan memasukan token <strong><?=$token?></strong>
                  </div>
                  <?php else:?>
                  <div class="alert alert-warning alert-dismissible">
                      <h4><i class="icon fa fa-warning"></i> Peringatan!</h4>
                        Data dan lamaran anda tidak dapat masuk pada sistem kami. File gagal disimpan</strong>
                  </div>
                  <?php endif;?>
              </div>
          </div>
      </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
