<style>
    td{
        padding: 5px;
    }
</style>

<!-- Main content -->
<section class="content">
  <div class="row">
      <?php foreach ($dataLowongan as $row):?>
    <div class="col-xs-3">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            <?=$row->judul?>
          </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table>
                <tr>
                    <td width="25%">Posisi </td>
                    <td>: <?=$this->M_mst_jabatan->getDetail($row->jabatanid)->nama." / ".$this->M_mst_peran->getDetail($row->peranid)->nama ?></td>
                </tr>
                <tr>
                    <td>Jumlah </td>
                    <td>: <?=$row->jumlah ?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin </td>
                    <td>: <?=($row->jenis_kelamin == 'l')?"Laki-laki":(($row->jenis_kelamin == 'p')?'Perempuan':'Semua') ?></td>
                </tr>
                <tr>
                    <td>Pendidikan </td>
                    <td>: <?=$row->jenjang_pendidikan ?></td>
                </tr>
                <tr>
                    <td>Nilai </td>
                    <td>: <?=$row->nilai_akhir ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?=$row->deskripsi?></td>
                </tr>
            </table>
        </div>
          <div class="box-footer">
              <a href="<?=site_url('FrontPage/register/'.$row->id)?>" class="btn btn-primary">JOIN</a>
          </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
      <?php endforeach;?>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->