
<!-- Main content -->
<section class="content">
  <div class="row">
      <div class="col-xs-12">
          <div class="box">
              <div class="box-body">
                  <div class="alert alert-warning alert-dismissible">
                      <h4><i class="icon fa fa-warning"></i> Peringatan!</h4>
                        Data pelamar tidak ditemukan pada sistem kami. Harap isi token dengan benar</strong>
                  </div>
              </div>
          </div>
      </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
