<style>
    td{
        padding: 2px;
    }
    .timeline>li>.fa{
        color: white;
    }
</style>

<!-- Main content -->
<section class="content">
  <div class="row">

      <div class="col-xs-4">
          <!-- The time line -->
          <ul class="timeline">
              <?php if($pelamar->statusid < 7):?>
              <?php foreach ($rowStatus as $row):?>
                  <?php if($row->id <= $pelamar->statusid):?>
                      <li>
                          <i class="fa fa-check bg-green-gradient"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header no-border lead"><strong><?=$row->nama?></strong></h3>
                            <div class="timeline-body">
                                Selamat Anda Telah Lulus Tahap Ini
                            </div>
                          </div>
                      </li>
                  <?php else: ?>
                      <li>
                          <i class="fa fa-minus bg-gray"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header no-border lead"><?=$row->nama?></h3>
                            <div class="timeline-body">
                                <?=$row->deskripsi?>
                                <br>
                                <?php if($pelamar->statusid == 4 && $row->id == 5):?>
                                    <a href="<?=site_url('FrontPage/onboarding/'.$pelamar->id);?>" class="btn btn-info"> test </a>
                                <?php endif;?>
                            </div>
                          </div>
                      </li>
                  <?php endif; ?>
              <?php endforeach?>
              <?php else:?>
              <li>
                  <i class="fa fa-times bg-red-gradient"></i>
                  <div class="timeline-item">
                      <h3 class="timeline-header no-border lead">TIDAK LOLOS</h3>
                      <div class="timeline-body">
                          Maaf anda tidak lolos dalam batch ini...
                      </div>
                  </div>
              </li>
              <?php endif;?>
          </ul>
      </div>
      <div class="col-xs-8">

          <!--      DETAIL-->
          <div class="col-xs-12">
              <div class="box box-default">
                  <div class="box-header">
                      <h3 class="box-title">
                          DETAIL
                      </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      <table border="0" width="100%">
                          <tr>
                              <td width="10%">Nama</td>
                              <td width="40">: <?=$pelamar->fullname?></td>
                              <td width="10%">Email</td>
                              <td width="40%">: <?=$pelamar->email?></td>
                          </tr>
                          <tr>
                              <td width="10%">Alamat</td>
                              <td width="40%">: <?=$pelamar->alamat?></td>
                              <td width="10%">No Telepon</td>
                              <td width="40">: <?=$pelamar->notelp?></td>
                          </tr>
                          <tr>
                              <td width="10%">Jenjang Pendidikan</td>
                              <td width="40%">: <?=$pelamar->jenjang_pendidikan?></td>
                              <td width="10%">Tempat Lahir</td>
                              <td width="40">: <?=$pelamar->tempat_lahir?></td>
                          </tr>
                          <tr>
                              <td width="10%">Instansi Pendidikan</td>
                              <td width="40%">: <?=$pelamar->instansi_pendidikan?></td>
                              <td width="10%">Tanggal Lahir</td>
                              <td width="40">: <?=$pelamar->tanggal_lahir?></td>
                          </tr>
                          <tr>
                              <td width="10%">Nilai</td>
                              <td width="40%">: <?=$pelamar->nilai_akhir?></td>
                              <td width="10%">Jenis Kelamin</td>
                              <td width="40">: <?=($pelamar->jenis_kelamin == "l")?'Laki-laki':'Perempuan'?></td>
                          </tr>
                      </table>
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->
          </div>
          <!--      PENDIDIKAN-->
          <div class="col-xs-6">
              <div class="box box-primary">
                  <div class="box-header">
                      <h3 class="box-title">
                          PENDIDIKAN
                      </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      <?php
                      if($pelamarPendidikan):
                      foreach ($pelamarPendidikan as $row):?>
                      <table border="0">
                          <tr>
                              <td>Pendidikan</td>
                              <td>: <?=$row->pendidikan?></td>
                          </tr>
                          <tr>
                              <td>Nama</td>
                              <td>: <?=$row->nama?></td>
                          </tr>
                          <tr>
                              <td>Jurusan</td>
                              <td>: <?=$row->jurusan?></td>
                          </tr>
                          <tr>
                              <td>Tahun Lulus</td>
                              <td>: <?=$row->tahun_lulus?></td>
                          </tr>
                          <tr>
                              <td>Nilai</td>
                              <td>: <?=$row->nilai?></td>
                          </tr>
                      </table>
                      <hr>
                      <?php
                      endforeach;
                      endif;
                      ?>
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->
          </div>
          <!--      PENGALAMAN-->
          <div class="col-xs-6">
              <div class="box box-success">
                  <div class="box-header">
                      <h3 class="box-title">
                          PENGALAMAN
                      </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      <?php
                      if($pelamarPengalaman):
                      foreach ($pelamarPengalaman as $row):?>
                          <table border="0">
                              <tr>
                                  <td>Organisasi</td>
                                  <td>: <?=$row->organisasi?></td>
                              </tr>
                              <tr>
                                  <td>Jabatan</td>
                                  <td>: <?=$row->jabatan?></td>
                              </tr>
                              <tr>
                                  <td>Masa Jabatan</td>
                                  <td>: <?=date('d/m/Y', strtotime($row->start_date))?> - <?=date('d/m/Y', strtotime($row->end_date))?></td>
                              </tr>
                          </table>
                          <hr>
                      <?php
                      endforeach;
                      endif;
                      ?>
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->
          </div>
      </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->