<style>
    td{
        padding: 5px;
    }

    .stepwizard-step p {
        margin-top: 0px;
        color:#666;
    }
    .stepwizard-row {
        display: table-row;
    }
    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }
    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important;
    }
    .stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
        opacity:1 !important;
        color:#bbb;
    }
    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content:" ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-index: 0;
    }
    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }
    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
</style>

<!-- Main content -->
<section class="content">
  <div class="row">
      <div class="col-xs-12">
          <div class="box">
              <div class="box-header">
                  <h3>
                      ON BOARDING <br>
                  </h3>
              </div>
              <div class="box-body">
                  <div class="stepwizard">
                      <div class="stepwizard-row setup-panel">
                          <?php
                          $no = 0;
                          $jumlah = count($rowMateri);
                          foreach ($rowMateri as $index => $item):
                              $no++;
                              ?>
                          <div class="stepwizard-step col-xs-<?=(1/$jumlah)*10?>">
                              <a href="#step-<?=$no?>" type="button" class="btn <?=($no == 1)?"btn-success":"btn-default"?> btn-circle" <?=($no == 1)?"":"disabled='disabled'"?>><?=$no?></a>
                              <p><small><?=($item->tipe == 1)?"Materi":"Soal"?></small></p>
                          </div>
                          <?php endforeach;?>
                          <div class="stepwizard-step col-xs-1">
                              <a href="#step-<?=$jumlah+1?>" type="button" class="btn btn-default btn-circle" disabled=''><?=$jumlah+1?></a>
                              <p><small>FINISH</small></p>
                          </div>
                      </div>
                  </div>

                  <?=form_open_multipart("FrontPage/onboarding_post/".$pelamarid)?>
                  <?php
                  $no = 0;
                  $jumlah = count($rowMateri);
                  foreach ($rowMateri as $index => $item):
                      $no++;
                      $tipe = ($item->tipe == 1)?"Materi":"Soal";
                      ?>
                      <div class="panel panel-primary setup-content" id="step-<?=$no?>">
                          <div class="panel-heading">
                              <h3 class="panel-title"><?=$tipe?></h3>
                          </div>
                          <div class="panel-body">
                              <h3><?=$item->judul?></h3>
                              <?php if($item->tipe == 1):?>
                              <p><?=$item->deskripsi?></p>
                              <?php else:
                                  $rowJawaban = $this->M_mst_materi_detail->getAllBy("materiid = $item->id");
                              foreach ($rowJawaban as $jawaban):
                                  ?>
                                  <div class="form-group col-md-12">
                                      <label><input type="radio" class="radio<?=$index?>" name="radio<?=$index?>" value="<?=$jawaban->istrue?>" ><?=$jawaban->jawaban;?></label>
                                  </div>
                              <?php
                              endforeach;
                              endif;?>
                          </div>
                          <div class="panel-footer" style="background: white">
                              <?php if($item->tipe == 1):?>
                                  <button class="btn btn-primary pull-right nextBtn" type="button" id="btn<?=$index?>">Next</button>
                              <?php else:?>
                                  <button class="btn btn-success" type="button" onclick="cek(<?=$index?>)">CHECK</button>
                                  <button class="btn btn-primary pull-right nextBtn" type="button" id="btn<?=$index?>" disabled>Next</button>
                              <?php endif;?>
                              <div class="clearfix"></div>
                          </div>
                      </div>
                  <?php endforeach;?>

                      <div class="panel panel-primary setup-content" id="step-<?=$jumlah+1?>">
                          <div class="panel-heading">
                              <h3 class="panel-title">FINISH</h3>
                          </div>
                          <div class="panel-body">
                              <p>Klik Submit, dapatkan kode token dan tunggu proses selanjutnya</p>
                              <button class="btn btn-success pull-right" type="submit">Submit!</button>
                          </div>
                      </div>
                  <?=form_close()?>
              </div>
          </div>
      </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->


<!-- Modal -->
<div id="modalPendidikan" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Pendidikan</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row form-horizontal">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="roleid" class="col-sm-4 control-label">Pendidikan</label>
                                <div class="col-sm-8">
                                    <select name="fpendidikan" id="fpendidikan" required class="form-control">
                                        <option value="">- pendidikan -</option>
                                        <option value="SD">SD/MI/Sederajat</option>
                                        <option value="SMP">SMP/MTs/Sederajat</option>
                                        <option value="SMA">SMA/SMK/MA/Sederajat</option>
                                        <option value="S1">S1</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fnama" class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="fnama" placeholder="fnama" name="fnama" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fjurusan" class="col-sm-4 control-label">Jurusan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="fjurusan" placeholder="fjurusan" name="fjurusan" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fnilai" class="col-sm-4 control-label">Nilai</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="fnilai" placeholder="fnilai" name="fnilai" value="" step="0.01">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ftahun_lulus" class="col-sm-4 control-label">Tahun Lulus</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="ftahun_lulus" placeholder="ftahun_lulus" name="ftahun_lulus" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="addPendididkan()">submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
<div id="modalPengalaman" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Pengalaman</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row form-horizontal">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="forganisasi" class="col-sm-4 control-label">Organisasi</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="forganisasi" placeholder="forganisasi" name="forganisasi" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fjabatan" class="col-sm-4 control-label">Jabatan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="fjabatan" placeholder="fjabatan" name="fjabatan" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fstart_date" class="col-sm-4 control-label">Start_date</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" id="fstart_date" placeholder="fstart_date" name="fstart_date" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fend_date" class="col-sm-4 control-label">End_date</label>
                                <div class="col-sm-8">
                                    <input type="date" class="form-control" id="fend_date" placeholder="fend_date" name="fend_date" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="addPengalaman()">submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-success').addClass('btn-default');
                $item.addClass('btn-success');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-success').trigger('click');
    });

    function cek(ini) {
        var isValid = $('.radio'+ini+':checked').val();
        if(isValid == 1){
            alert("SELAMAT ANDA DAPAT MELANJUTKAN TEST ");
            $('#btn'+ini).prop('disabled',false);
        }
        else{
            alert("SALAH! Harap baca materi dengan teliti, dan coba lagi");
        }
    }
</script>