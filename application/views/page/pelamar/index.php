<?php
$iduser = $this->session->userdata("id");
$user = $this->M_user->getDetail($iduser);
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pelamar
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=site_url('Lowongan');?>"><i class="fa fa-briefcase"></i> Lowongan</a></li>
        <li><a href=""> Pelamar </a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Tabel Data <b><?=$dataLowongan->judul?></b>
                    </h3>
                    <div class="pull-right col-md-2">
                        <?=form_open("Pelamar/filter/".$dataLowongan->id); ?>
                        <div class="input-group">
                            <select name="statusid" id="fstatusid" required class="form-control">
                                <option value="ALL" <?=($statusid == "ALL") ? "selected" : ""?>>SEMUA</option>
                                <?php foreach($rowStatus as $row):?>
                                    <option <?=($statusid == $row->id) ? "selected" : ""?> value="<?=$row->id?>" ><?=$row->nama?></option>
                                <?php endforeach;?>
                            </select>
                            <div class="input-group-btn">
                                <?=form_submit("btnsubmit", "filter","class='btn btn-success'");?>
                            </div>
                            <!-- /btn-group -->
                        </div>
                        <?=form_close();?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body"><div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#prioritas" data-toggle="tab" aria-expanded="true">Prioritas</a></li>
                        <li class=""><a href="#nonprioritas" data-toggle="tab" aria-expanded="false">Non Prioritas</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="prioritas">
                            <table id="" class="table table-bordered table-striped example0">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Lowongan</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>No Telp</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Umur</th>
                                    <th>Jenjang Pendidikan</th>
                                    <th>Sekolah/Universitas</th>
                                    <th>Tahun Lulus</th>
                                    <th>Nilai Ijazah</th>
                                    <th>CV</th>
                                    <th>Status</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $no = 1;
                                foreach ($rowDataPrioritas as $row) :
                                    ?>
                                    <tr>
                                        <td><?=$no++;?></td>
                                        <td><?=$dataLowongan->judul;?></td>
                                        <td><?=$row->fullname;?></td>
                                        <td><?=$row->email;?></td>
                                        <td><?=$row->notelp;?></td>
                                        <td><?=$row->jenis_kelamin;?></td>
                                        <td><?=$row->umur;?></td>
                                        <td><?=$row->jenjang_pendidikan;?></td>
                                        <td><?=$row->instansi_pendidikan;?></td>
                                        <td><?=$row->tahun_lulus;?></td>
                                        <td><?=$row->nilai_akhir;?></td>
                                        <td>
                                            <a href="<?=site_url('extras/upload/cv/'.$row->cv);?>" class="btn btn-xs btn-success"><i class="fa fa-download" title="download"></i> cv</a>
                                        </td>
                                        <td><?=$this->M_mst_status->getDetail($row->statusid)->nama;?></td>
                                        <td>
                                            <a href="<?=site_url('Pelamar/detailPage/'.$row->id);?>" class="btn btn-xs btn-info"><i class="fa fa-info" title="detail"></i></a>
                                            <?php if($user->roleid == "1"):?>
                                                <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalStatus" onclick="getDetail(this)" class="btn btn-xs btn-warning" title="set status"><i class="fa fa-tag"></i></a>
                                            <?php endif;?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="nonprioritas">

                            <table id="" class="table table-bordered table-striped example0">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Lowongan</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>No Telp</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Umur</th>
                                    <th>Jenjang Pendidikan</th>
                                    <th>Sekolah/Universitas</th>
                                    <th>Tahun Lulus</th>
                                    <th>Nilai Ijazah</th>
                                    <th>CV</th>
                                    <th>Status</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $no = 1;
                                foreach ($rowDataNonPrioritas as $row) :
                                    ?>
                                    <tr>
                                        <td><?=$no++;?></td>
                                        <td><?=$dataLowongan->judul;?></td>
                                        <td><?=$row->fullname;?></td>
                                        <td><?=$row->email;?></td>
                                        <td><?=$row->notelp;?></td>
                                        <td><?=$row->jenis_kelamin;?></td>
                                        <td><?=$row->umur;?></td>
                                        <td><?=$row->jenjang_pendidikan;?></td>
                                        <td><?=$row->instansi_pendidikan;?></td>
                                        <td><?=$row->tahun_lulus;?></td>
                                        <td><?=$row->nilai_akhir;?></td>
                                        <td>
                                            <a href="<?=site_url('extras/upload/cv/'.$row->cv);?>" class="btn btn-xs btn-success"><i class="fa fa-download" title="download"></i> cv</a>
                                        </td>
                                        <td><?=$this->M_mst_status->getDetail($row->statusid)->nama;?></td>
                                        <td>
                                            <a href="<?=site_url('Pelamar/detailPage/'.$row->id);?>" class="btn btn-xs btn-info"><i class="fa fa-info" title="detail"></i></a>
                                            <?php if($user->roleid == "1"):?>
                                                <a href="" data-id="<?=$row->id?>" data-toggle="modal" data-target="#modalStatus" onclick="getDetail(this)" class="btn btn-xs btn-warning" title="set status"><i class="fa fa-tag"></i></a>
                                            <?php endif;?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->



<!-- Modal -->
<div id="modalStatus" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Status</h4>
            </div>
            <?=form_open("Pelamar/setStatus","class='form-horizontal'");
            ?>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fullname" class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control lowonganid" placeholder="lowonganid" name="lowonganid" value="<?=$dataLowongan->id?>">
                                    <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                                    <input type="text" class="form-control fullname" placeholder="fullname" name="fullname" value="" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="infotambahan" class="col-sm-4 control-label">Info Tambahan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control infotambahan" placeholder="infotambahan" name="infotambahan" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="statusid" class="col-sm-4 control-label">Status</label>
                                <div class="col-sm-8">
                                    <select name="statusid" id="statusid" required class="form-control">
                                        <option value="">- status -</option>
                                        <?php foreach($rowStatus as $row):?>
                                            <option value="<?=$row->id?>" ><?=$row->nama?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>


<script>
    function getDetail(ini) {
        var id = $(ini).attr('data-id');
        $.ajax({
            type: 'GET',
            url: "<?=base_url('');?>Pelamar/detail/"+id,
            success: function (data) {
                //Do stuff with the JSON data
                console.log(data);
                $('.id').val(id).hide();
                $('.fullname').val(data.fullname);
                $('#statusid').val(data.statusid);
            }
        });
    }

    function clearForm() {
        $('#id').val("");
        $('#fullname').val("");
        $('#statusid').val("");
    }
</script>