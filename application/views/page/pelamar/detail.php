
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Pelamar
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=site_url('Lowongan');?>"><i class="fa fa-briefcase"></i> Lowongan</a></li>
    <li><a href="<?=site_url('Pelamar/index/'.$data->lowonganid);?>"><i class="fa fa-users"></i> Pelamar</a></li>
    <li><a href=""> Detail Pelamar </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
<!--        DETAIL PELAMAR-->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Detail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
            </div>
        </div>
    </div>
      <!--Detail Pendidikan-->
    <div class="col-xs-6">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Pendidikan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
            </div>
        </div>
    </div>
      <!--Detail Pengalaman-->
    <div class="col-xs-6">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Pengalaman</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <a href="<?=site_url('Pelamar/index/'.$data->lowonganid);?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
    </div>

    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
