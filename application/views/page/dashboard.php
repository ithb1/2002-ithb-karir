<?php
    $iduser = $this->session->userdata("id");
    $user = $this->M_user->getDetail($iduser);
?>

<style>
  .batasbawah{
    height: 400px !important;
  }
</style>

<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>


<section class="content">
    <?php if($user->roleid == '1'):?>
    <div class="alert alert-warning alert-dismissible">
        <h4><i class="icon fa fa-warning"></i> Warning!</h4>
        <strong><?=count($rowLowongan)?></strong> Lowongan baru butuh approval
    </div>
    <?php endif;?>

    <?php if($user->roleid == '2'):?>
    <?php $this->load->view("page/dashboardDepartemen");?>
    <?php endif;?>
</section>

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?=base_url('extras/');?>plugins/morris/morris.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    </script>